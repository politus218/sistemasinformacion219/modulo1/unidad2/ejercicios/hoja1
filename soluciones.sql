﻿/** 
  HOJA 1 CONSULTA DE SELECCIÓN	1 
  
  Las consultas siguientes nos permiten relacionarnos con 
  la sintaxis de las consultas basicas de SQL.

  Ademas iremos utilizando el algebra relacional.

**/


/*
  Consulta 1: 
  Listar las edades de los ciclistas (sin repetidos)
*/

  SELECT 
    DISTINCT c.edad  -- PROYECCION
  FROM 
    ciclista c;      -- TABLA A UTILIZAR

/*
  Consulta 2:
  Listar las edades de los ciclistas de Artiach
*/
  
  SELECT DISTINCT
    c.edad -- proyeccion
  FROM  
    ciclista c  -- tablas a utilizar
  WHERE
    c.nomequipo='Artiach';  -- seleccion

/*
  Consulta 3:
  Listar las edades de los ciclistas de Artiach o de Amore Vita
*/

  SELECT DISTINCT
    c.edad -- proyeccion
  FROM 
    ciclista c -- tablas
  WHERE 
    c.nomequipo='Artiach'
    OR
    c.nomequipo='Amore Vita'; -- seleccion

  -- utilizo el operador IN
  SELECT DISTINCT
    c.edad -- proyeccion
  FROM 
    ciclista c -- tablas
  WHERE 
    c.nomequipo IN ('Artiach','Amore Vita'); -- seleccion

  -- utilizo el operador UNION
  SELECT DISTINCT
    c.edad  
  FROM 
    ciclista c
  WHERE 
    c.nomequipo='Artiach'
  UNION 
  SELECT DISTINCT
    c.edad  
  FROM 
    ciclista c
  WHERE 
    c.nomequipo='Amore Vita';

/*
  Consulta 4:
  Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.
*/
  
-- operador OR
  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.edad<25 OR c.edad>30;

-- operador UNION
SELECT 
  c.dorsal 
FROM 
  ciclista c 
WHERE 
  c.edad<25

UNION 

SELECT 
    c.dorsal 
FROM 
    ciclista c 
WHERE 
    c.edad>30;

/*
  Consulta 5:
  Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean de Banesto.
*/

SELECT 
  c.dorsal 
FROM 
  ciclista c
WHERE
  c.edad BETWEEN 28 AND 32
  AND
  c.nomequipo='Banesto';

-- realizarlo mediante el operador interseccion
SELECT C2.dorsal
FROM 
  (SELECT c.dorsal FROM ciclista c 
    WHERE c.edad BETWEEN 28 AND 32) AS c1 
JOIN 
  (SELECT c.dorsal FROM ciclista c 
    WHERE c.nomequipo='Banesto') AS C2
ON 
  c1.dorsal=C2.dorsal;

/* 
  Consulta 6:
  Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8
*/

  SELECT 
    nombre
  FROM 
    ciclista c
  WHERE
    CHAR_LENGTH(nombre)>8;

/*
  Consulta 7:
  Listame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayusculas que debe mostrar el nombre en mayúsculas
*/
  
  SELECT 
    c.nombre, c.dorsal, UPPER(nombre) AS `nombre Mayusculas`
  FROM 
    ciclista c;

/*
  Consulta 8:
  Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa
*/
  
  SELECT DISTINCT
    l.dorsal  
  FROM 
    lleva l 
  WHERE 
    l.código = 'MGE';

/*
  Consulta 9:
  Listar el nombre de los puertos cuya altura sea mayor que 1500
*/

  SELECT 
    p.nompuerto 
  FROM 
    puerto p 
  WHERE 
    p.altura >1500;


/*
  Consulta 10:
  Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000
*/
  
  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000 
    OR 
    p.pendiente>8;


  -- REALIZAR A TRAVES DE UNA UNION
  -- C1
  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000;
  
  -- C2
  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.pendiente>8; 
  
  -- C1 UNION C2
  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000
  
  UNION 

   SELECT DISTINCT
      p.dorsal
   FROM 
      puerto p 
   WHERE 
      p.pendiente>8; 


/*
  Consulta 11:
  Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000
*/

  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000 
    AND  
    p.pendiente>8;

-- esto es una interseccion y no es lo mismo que lo anterior

SELECT c1.dorsal
  FROM 
    (SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura BETWEEN 1800 AND 3000) AS c1
  JOIN 
    (SELECT DISTINCT p.dorsal FROM puerto p WHERE p.pendiente>8) AS c2 
  ON c1.dorsal=c2.dorsal;
    



