﻿-- ----------------------------------------------------------------------
-- MySQL Migration Toolkit
-- SQL Create Script
-- ----------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

CREATE DATABASE IF NOT EXISTS `ciclistas`
  CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ciclistas`;
-- -------------------------------------
-- Tables

DROP TABLE IF EXISTS `ciclistas`.`ciclista`;
CREATE TABLE `ciclistas`.`ciclista` (
  `dorsal` SMALLINT(5) NOT NULL,
  `nombre` VARCHAR(30) NOT NULL,
  `edad` SMALLINT(5) NULL,
  `nomequipo` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`dorsal`),
  INDEX `equipociclista` (`nomequipo`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`equipo`;
CREATE TABLE `ciclistas`.`equipo` (
  `nomequipo` VARCHAR(25) NOT NULL,
  `director` VARCHAR(30) NULL,
  PRIMARY KEY (`nomequipo`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`etapa`;
CREATE TABLE `ciclistas`.`etapa` (
  `numetapa` SMALLINT(5) NOT NULL,
  `kms` SMALLINT(5) NOT NULL,
  `salida` VARCHAR(35) NOT NULL,
  `llegada` VARCHAR(35) NOT NULL,
  `dorsal` SMALLINT(5) NULL,
  PRIMARY KEY (`numetapa`),
  INDEX `ciclistaetapa` (`dorsal`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`lleva`;
CREATE TABLE `ciclistas`.`lleva` (
  `dorsal` SMALLINT(5) NOT NULL,
  `numetapa` SMALLINT(5) NOT NULL,
  `código` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`numetapa`, `código`),
  INDEX `ciclistallevar` (`dorsal`),
  INDEX `etapallevar` (`numetapa`),
  INDEX `maillotllevar` (`código`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`maillot`;
CREATE TABLE `ciclistas`.`maillot` (
  `código` VARCHAR(3) NOT NULL,
  `tipo` VARCHAR(30) NOT NULL,
  `color` VARCHAR(20) NOT NULL,
  `premio` INT(10) NOT NULL,
  PRIMARY KEY (`código`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`puerto`;
CREATE TABLE `ciclistas`.`puerto` (
  `nompuerto` VARCHAR(35) NOT NULL,
  `altura` SMALLINT(5) NOT NULL,
  `categoria` VARCHAR(1) NOT NULL,
  `pendiente` DOUBLE(15, 5) NULL,
  `numetapa` SMALLINT(5) NOT NULL,
  `dorsal` SMALLINT(5) NULL,
  PRIMARY KEY (`nompuerto`),
  INDEX `ciclistapuerto` (`dorsal`),
  INDEX `etapapuerto` (`numetapa`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;



SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------------------------------------------------
-- EOF


-- Listar las edades de los ciclistas sin repetirlos.

  SELECT DISTINCT c.edad FROM ciclista c;

-- Listar las edades de los ciclistas de Artiach.

  SELECT DISTINCT c.edad FROM ciclista c
  WHERE c.nomequipo ='Artiach';

-- Listar las edades de los ciclistas de Artiach o de Amore Vita.

  SELECT DISTINCT c.edad FROM ciclista c
  WHERE c.nomequipo ='Artiach' OR c.nomequipo ='Amore Vita';

-- Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.

  SELECT c.dorsal FROM ciclista c
  WHERE c.edad <25 OR c.edad >30;

-- Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además sean del equipo Banesto.

  SELECT c.dorsal FROM ciclista c
  WHERE edad BETWEEN 28 AND 32 AND c.nomequipo= 'Banesto';
  
-- El nombre de los ciclistas que el nº de cracteres por el nombre sea mayor de 8.

  SELECT * FROM ciclista c
  WHERE c.nombre > 8;

-- Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas.

SELECT UPPER(RTRIM(c.nombre)), c.dorsal FROM ciclista c;

-- Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa.

  SELECT l.dorsal FROM lleva l
  WHERE l.código= 'MGE';

-- Listar el nombre de los puertos cuya altura sea mayor que 1500.
  
  SELECT * FROM puerto p
  WHERE p.altura > 1500;   

--  Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 Y 3000.

  SELECT p.dorsal FROM puerto p
  WHERE p.pendiente >8 OR p.altura BETWEEN 1800 AND 3000;

--  Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000.

  SELECT p.dorsal FROM puerto p
  WHERE p.pendiente >8 AND p.altura BETWEEN 1800 AND 3000;
  

     